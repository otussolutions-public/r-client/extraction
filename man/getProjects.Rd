\name{getProjects}
\alias{getProjects}
\title{
Function getProjects
}
\description{
Get randomization projects by user id
}
\usage{
getProjects()
}
\value{
  Return user's randomization projects
}
\examples{
  getProjects()
}
