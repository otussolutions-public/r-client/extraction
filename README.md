# Otus Randomization
This library aims to provide access to the randomization service programmatically, without the need for iteration via visual interface.
Through it is possible to perform all available actions via interface.


## Install dependencies
You must first install the necessary dependencies to use the package.
##
```python
install.packages("devtools")
remotes::install_bitbucket("otus-solutions/r-package")
```

## Load library
Before you can start using the library it must be loaded into context.
##
```python
library(otusRandomization)
```

## Authentication
All library functionality is available from service authentication, so login validation must be performed first.
##
```python
service <- otusRandomization::auth("URL-SERVICE", "user", "password")
service$getProjects()

```


